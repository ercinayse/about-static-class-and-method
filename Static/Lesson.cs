﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Static
{
    public struct Lesson
    {
        public string Name { get; set; }

        public decimal Midterm { get; set; }
        public decimal Final { get; set; }

    }
}
