﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Static
{
    public class Student
    {
        public Student()
        {
            StudentProcess.NumberofStudent++;
        }
        public string Name { get; set; }
        public string LastName { get; set; }

        public string ID { get; set; }

        public static List<Lesson> les = new List<Lesson>
        {
            new Lesson { Name="MTM1230-IntroductionProgramming"},
            new Lesson {Name= "MTM4120-ObjectOriented" },
            new Lesson {Name="MTM3350-StructureQueryLanguage"},
            new Lesson {Name="MAT1001-Mathematics"}
        };


    }
}
