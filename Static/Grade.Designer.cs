﻿namespace Static
{
    partial class Grade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelfinal = new System.Windows.Forms.Label();
            this.labelmidterm = new System.Windows.Forms.Label();
            this.labellesson = new System.Windows.Forms.Label();
            this.comboBoxlesson = new System.Windows.Forms.ComboBox();
            this.numericUpDownmidterm = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownfinal = new System.Windows.Forms.NumericUpDown();
            this.buttoncalculate = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownmidterm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownfinal)).BeginInit();
            this.SuspendLayout();
            // 
            // labelfinal
            // 
            this.labelfinal.AutoSize = true;
            this.labelfinal.Location = new System.Drawing.Point(425, 43);
            this.labelfinal.Name = "labelfinal";
            this.labelfinal.Size = new System.Drawing.Size(29, 13);
            this.labelfinal.TabIndex = 2;
            this.labelfinal.Text = "Final";
            // 
            // labelmidterm
            // 
            this.labelmidterm.AutoSize = true;
            this.labelmidterm.Location = new System.Drawing.Point(258, 43);
            this.labelmidterm.Name = "labelmidterm";
            this.labelmidterm.Size = new System.Drawing.Size(44, 13);
            this.labelmidterm.TabIndex = 3;
            this.labelmidterm.Text = "Midterm";
            // 
            // labellesson
            // 
            this.labellesson.AutoSize = true;
            this.labellesson.Location = new System.Drawing.Point(54, 43);
            this.labellesson.Name = "labellesson";
            this.labellesson.Size = new System.Drawing.Size(46, 13);
            this.labellesson.TabIndex = 4;
            this.labellesson.Text = "Lessons";
            // 
            // comboBoxlesson
            // 
            this.comboBoxlesson.FormattingEnabled = true;
            this.comboBoxlesson.Location = new System.Drawing.Point(57, 75);
            this.comboBoxlesson.Name = "comboBoxlesson";
            this.comboBoxlesson.Size = new System.Drawing.Size(158, 21);
            this.comboBoxlesson.TabIndex = 6;
            // 
            // numericUpDownmidterm
            // 
            this.numericUpDownmidterm.Location = new System.Drawing.Point(261, 75);
            this.numericUpDownmidterm.Name = "numericUpDownmidterm";
            this.numericUpDownmidterm.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownmidterm.TabIndex = 7;
            // 
            // numericUpDownfinal
            // 
            this.numericUpDownfinal.Location = new System.Drawing.Point(428, 75);
            this.numericUpDownfinal.Name = "numericUpDownfinal";
            this.numericUpDownfinal.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownfinal.TabIndex = 8;
            // 
            // buttoncalculate
            // 
            this.buttoncalculate.Location = new System.Drawing.Point(595, 72);
            this.buttoncalculate.Name = "buttoncalculate";
            this.buttoncalculate.Size = new System.Drawing.Size(75, 23);
            this.buttoncalculate.TabIndex = 9;
            this.buttoncalculate.Text = "Calculate";
            this.buttoncalculate.UseVisualStyleBackColor = true;
            this.buttoncalculate.Click += new System.EventHandler(this.buttoncalculate_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(57, 127);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(921, 362);
            this.listView1.TabIndex = 10;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Lesson";
            this.columnHeader1.Width = 252;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Midterm";
            this.columnHeader2.Width = 79;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Final";
            this.columnHeader3.Width = 79;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Average";
            this.columnHeader4.Width = 71;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Grade";
            this.columnHeader5.Width = 70;
            // 
            // Grade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 501);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.buttoncalculate);
            this.Controls.Add(this.numericUpDownfinal);
            this.Controls.Add(this.numericUpDownmidterm);
            this.Controls.Add(this.comboBoxlesson);
            this.Controls.Add(this.labelfinal);
            this.Controls.Add(this.labelmidterm);
            this.Controls.Add(this.labellesson);
            this.Name = "Grade";
            this.Text = "Grade";
            this.Load += new System.EventHandler(this.Grade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownmidterm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownfinal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelfinal;
        private System.Windows.Forms.Label labelmidterm;
        private System.Windows.Forms.Label labellesson;
        private System.Windows.Forms.ComboBox comboBoxlesson;
        private System.Windows.Forms.NumericUpDown numericUpDownmidterm;
        private System.Windows.Forms.NumericUpDown numericUpDownfinal;
        private System.Windows.Forms.Button buttoncalculate;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
    }
}