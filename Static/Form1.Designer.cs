﻿namespace Static
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelname = new System.Windows.Forms.Label();
            this.labelsurname = new System.Windows.Forms.Label();
            this.labelsoftskill = new System.Windows.Forms.Label();
            this.labelId = new System.Windows.Forms.Label();
            this.textBoxname = new System.Windows.Forms.TextBox();
            this.textBoxlastname = new System.Windows.Forms.TextBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.comboBoxsoft = new System.Windows.Forms.ComboBox();
            this.buttonsave = new System.Windows.Forms.Button();
            this.labelnumberofstudent = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelresult = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Name1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Lastname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Lessons = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttongrade = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelname
            // 
            this.labelname.AutoSize = true;
            this.labelname.Location = new System.Drawing.Point(30, 63);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(35, 13);
            this.labelname.TabIndex = 0;
            this.labelname.Text = "Name";
            // 
            // labelsurname
            // 
            this.labelsurname.AutoSize = true;
            this.labelsurname.Location = new System.Drawing.Point(161, 63);
            this.labelsurname.Name = "labelsurname";
            this.labelsurname.Size = new System.Drawing.Size(53, 13);
            this.labelsurname.TabIndex = 0;
            this.labelsurname.Text = "Lastname";
            // 
            // labelsoftskill
            // 
            this.labelsoftskill.AutoSize = true;
            this.labelsoftskill.Location = new System.Drawing.Point(420, 63);
            this.labelsoftskill.Name = "labelsoftskill";
            this.labelsoftskill.Size = new System.Drawing.Size(79, 13);
            this.labelsoftskill.TabIndex = 0;
            this.labelsoftskill.Text = "Softskill Course";
            // 
            // labelId
            // 
            this.labelId.AutoSize = true;
            this.labelId.Location = new System.Drawing.Point(288, 63);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(18, 13);
            this.labelId.TabIndex = 0;
            this.labelId.Text = "ID";
            // 
            // textBoxname
            // 
            this.textBoxname.Location = new System.Drawing.Point(33, 94);
            this.textBoxname.Name = "textBoxname";
            this.textBoxname.Size = new System.Drawing.Size(100, 20);
            this.textBoxname.TabIndex = 0;
            // 
            // textBoxlastname
            // 
            this.textBoxlastname.Location = new System.Drawing.Point(164, 94);
            this.textBoxlastname.Name = "textBoxlastname";
            this.textBoxlastname.Size = new System.Drawing.Size(100, 20);
            this.textBoxlastname.TabIndex = 1;
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(291, 94);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(100, 20);
            this.textBoxID.TabIndex = 2;
            // 
            // comboBoxsoft
            // 
            this.comboBoxsoft.FormattingEnabled = true;
            this.comboBoxsoft.Location = new System.Drawing.Point(423, 94);
            this.comboBoxsoft.Name = "comboBoxsoft";
            this.comboBoxsoft.Size = new System.Drawing.Size(214, 21);
            this.comboBoxsoft.TabIndex = 3;
            // 
            // buttonsave
            // 
            this.buttonsave.Location = new System.Drawing.Point(664, 94);
            this.buttonsave.Name = "buttonsave";
            this.buttonsave.Size = new System.Drawing.Size(75, 23);
            this.buttonsave.TabIndex = 5;
            this.buttonsave.Text = "Save";
            this.buttonsave.UseVisualStyleBackColor = true;
            this.buttonsave.Click += new System.EventHandler(this.buttonsave_Click);
            // 
            // labelnumberofstudent
            // 
            this.labelnumberofstudent.AutoSize = true;
            this.labelnumberofstudent.Location = new System.Drawing.Point(33, 13);
            this.labelnumberofstudent.Name = "labelnumberofstudent";
            this.labelnumberofstudent.Size = new System.Drawing.Size(104, 13);
            this.labelnumberofstudent.TabIndex = 5;
            this.labelnumberofstudent.Text = "Number of Students:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 6;
            // 
            // labelresult
            // 
            this.labelresult.AutoSize = true;
            this.labelresult.Location = new System.Drawing.Point(143, 13);
            this.labelresult.Name = "labelresult";
            this.labelresult.Size = new System.Drawing.Size(0, 13);
            this.labelresult.TabIndex = 7;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Name1,
            this.Lastname,
            this.ID,
            this.Lessons});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(33, 144);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(902, 350);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Name1
            // 
            this.Name1.Text = "Name";
            this.Name1.Width = 77;
            // 
            // Lastname
            // 
            this.Lastname.Text = "Lastname";
            this.Lastname.Width = 77;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 114;
            // 
            // Lessons
            // 
            this.Lessons.Text = "Softskill";
            this.Lessons.Width = 204;
            // 
            // buttongrade
            // 
            this.buttongrade.Location = new System.Drawing.Point(760, 94);
            this.buttongrade.Name = "buttongrade";
            this.buttongrade.Size = new System.Drawing.Size(143, 23);
            this.buttongrade.TabIndex = 9;
            this.buttongrade.Text = "Calculate Grade";
            this.buttongrade.UseVisualStyleBackColor = true;
            this.buttongrade.Click += new System.EventHandler(this.buttongrade_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.buttonsave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 506);
            this.Controls.Add(this.buttongrade);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.labelresult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelnumberofstudent);
            this.Controls.Add(this.buttonsave);
            this.Controls.Add(this.comboBoxsoft);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.textBoxlastname);
            this.Controls.Add(this.textBoxname);
            this.Controls.Add(this.labelsoftskill);
            this.Controls.Add(this.labelId);
            this.Controls.Add(this.labelsurname);
            this.Controls.Add(this.labelname);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labelsurname;
        private System.Windows.Forms.Label labelsoftskill;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.TextBox textBoxname;
        private System.Windows.Forms.TextBox textBoxlastname;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.ComboBox comboBoxsoft;
        private System.Windows.Forms.Button buttonsave;
        private System.Windows.Forms.Label labelnumberofstudent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelresult;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Name1;
        private System.Windows.Forms.ColumnHeader Lastname;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Lessons;
        private System.Windows.Forms.Button buttongrade;
    }
}

