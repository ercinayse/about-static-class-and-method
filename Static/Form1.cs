﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Static
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

           
            foreach (var item in Student.les)
            {
                comboBoxsoft.Items.Add(item.Name);
            }

            comboBoxsoft.SelectedIndex = 0;

           
            


            
        }

        private void buttonsave_Click(object sender, EventArgs e)
        {
            

            Student stu = new Student();
            stu.Name = textBoxname.Text;
            stu.LastName = textBoxlastname.Text;
            stu.ID = textBoxID.Text;
            ListViewItem lvi = new ListViewItem();
            lvi.Text = textBoxname.Text;
            lvi.SubItems.Add(textBoxlastname.Text);
            lvi.SubItems.Add(textBoxID.Text);
            lvi.SubItems.Add(comboBoxsoft.SelectedItem.ToString());
            
            listView1.Items.Add(lvi);
            labelresult.Text = StudentProcess.NumberofStudent.ToString();
            
            

            foreach (Control ctrl in this.Controls)
            {
                if(ctrl is TextBox)
                {
                    ((TextBox)ctrl).Text = "";
                }
            }
        }

        private void buttongrade_Click(object sender, EventArgs e)
        {
            Grade form = new Grade();
            form.Show();
        }
    }
}
