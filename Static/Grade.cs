﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Static
{
    public partial class Grade : Form
    {
        public Grade()
        {
            InitializeComponent();
        }

        private void Grade_Load(object sender, EventArgs e)
        {
            foreach (var item in Student.les)
            {
                comboBoxlesson.Items.Add(item.Name);
            }

            comboBoxlesson.SelectedIndex = 0;
        }

        private void buttoncalculate_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem();
            decimal midterm = numericUpDownmidterm.Value;
            decimal final = numericUpDownfinal.Value;
            lvi.Text = comboBoxlesson.SelectedItem.ToString();
            lvi.SubItems.Add(midterm.ToString());
            lvi.SubItems.Add(final.ToString());
            decimal average = StudentProcess.CalculateAverage(midterm, final);
            lvi.SubItems.Add(average.ToString());
            lvi.SubItems.Add(StudentProcess.CalculateMark(average));
            listView1.Items.Add(lvi);

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is NumericUpDown)
                {
                    ((NumericUpDown)ctrl).Value= 0;
                }
            }

        }
    }
}
