﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Static
{
    public static class StudentProcess
    {
        public static int NumberofStudent { get; set; }

        public static decimal CalculateAverage(decimal midterm,decimal final)
        {
            return (midterm * 0.4m) + (final * 0.6m);
        }

        public static string CalculateMark(decimal total)
        {
            if (total >= 0 && total < 50)
                return "FF";
            else if (total >= 50 && total < 70)
                return "BB";
            else
                return "AA";
        }
    }
}
